import React, { Component } from "react";
import Header from "./Header";

export default class LifeCycle extends Component {
  state = {
    like: 1,
  };
  componentDidMount() {
    console.log("LIFE_CYCLE  componentDidMount ");
    // MOUTING - chỉ được chạy 1 lần duy nhất ( sau khi render chạy )
    // thường để gọi api - login
  }

  handlePlusLike = () => {
    this.setState({ like: this.state.like + 1 });
  };
  shouldComponentUpdate(nextProps, nextState) {
    console.log(`  🚀 __ file: LifeCycle.js:18 __ nextState`, nextState);
    console.log("current", this.state);
    // mặc định sẽ retrun true
    if (nextState.like == 2) {
      return false;
    }
    return true;
  }
  render() {
    console.log("LIFE_CYCLE render");
    return (
      <div className="p-5 bg-primary text-white">
        {this.state.like < 5 && <Header />} <p>LifeCycle</p>
        <span className="display-4">{this.state.like}</span>
        <button onClick={this.handlePlusLike} className="btn btn-success">
          Plus like
        </button>
      </div>
    );
  }
  componentDidUpdate() {
    console.log("updated");
  }
}

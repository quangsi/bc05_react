let initialState = {
  number: 100,
};

export const numberReducer = (state = initialState, action) => {
  switch (action.type) {
    case "TANG_SO_LUONG": {
      state.number++;
      return { ...state };
    }
    case "GIAM_SO_LUONG": {
      state.number = state.number - action.payload;
      return { ...state };
    }
    default:
      return state;
  }
};

import { combineReducers } from "redux";
import { shoeReducer } from "./shoeReducer";

export const rootReducer_Shoe_Shop = combineReducers({ shoeReducer });

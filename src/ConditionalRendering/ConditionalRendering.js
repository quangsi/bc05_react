import React, { Component } from "react";

export default class ConditionalRendering extends Component {
  state = {
    isLogin: true,
  };
  handleLogin = () => {
    this.setState({
      isLogin: true,
    });
  };
  handleLogout = () => {
    this.setState({
      isLogin: false,
    });
  };
  renderContent = () => {
    if (this.state.isLogin) {
      // đã đăng nhập
      return (
        <div>
          <p>Hi Alice</p>
          <button onClick={this.handleLogout} className="btn btn-danger">
            Logout
          </button>
        </div>
      );
    } else {
      // chưa đăng nhập
      return (
        <div>
          <p>Welcome . Please login</p>
          <button onClick={this.handleLogin} className="btn btn-success">
            Login
          </button>
        </div>
      );
    }
  };

  render() {
    return <div>{this.renderContent()}</div>;
  }
}

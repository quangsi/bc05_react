import { nanoid } from "nanoid";
import React, { Component } from "react";

export default class UserForm extends Component {
  constructor(props) {
    super(props);
    this.inputRef = React.createRef();
  }
  state = {
    user: {
      name: "",
      password: "",
    },
  };
  componentDidMount() {
    // this.inputRef.current.value = "Alice";
    // this.inputRef.current.style.color = "red";
  }
  handleUserSubmit = () => {
    let newUser = { ...this.state.user };
    newUser.id = nanoid(5);
    this.props.handleUserAdd(newUser);
  };

  // 4 dòng
  handleGetUserForm = (e) => {
    console.log(e.target.name);
    let { value, name: key } = e.target;
    // name: key , đổi tên name thành key
    let cloneUser = { ...this.state.user };
    cloneUser[key] = value;
    this.setState({ user: cloneUser }, () => {
      console.log(this.state.user);
    });
  };
  componentWillReceiveProps(nextProps) {
    console.log(`  🚀 __ file: UserForm.js:37 __ nextProps`, nextProps);
    if (nextProps.userEdited != null) {
      this.setState({ user: nextProps.userEdited });
    }
  }

  render() {
    return (
      <div>
        <div className="form-group">
          <input
            onChange={this.handleGetUserForm}
            value={this.state.user.name}
            ref={this.inputRef}
            type="text"
            className="form-control"
            name="name"
            placeholder="Username"
          />
        </div>
        <div className="form-group">
          <input
            onChange={this.handleGetUserForm}
            value={this.state.user.password}
            type="text"
            className="form-control"
            name="password"
            placeholder="Password"
          />
        </div>
        <button onClick={this.handleUserSubmit} className="btn btn-warning">
          Add user
        </button>
      </div>
    );
  }
}

let user = {
  name: "alice",
  age: "bob",
};
user.name = "bob";
let key = "age";

user[key] = "bob";

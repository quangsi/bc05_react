import React, { Component } from "react";

export default class UserTable extends Component {
  renderUserTable = () => {
    return this.props.userList.map((item, key) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.password}</td>
          <td>
            <button
              onClick={() => {
                this.props.handleUserRemove(item.id);
              }}
              className="btn btn-danger mr-2"
            >
              Delete
            </button>
            <button
              onClick={() => {
                this.props.handleEditUser(item);
              }}
              className="btn btn-warning"
            >
              Edit
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Password</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>{this.renderUserTable()}</tbody>
      </table>
    );
  }
}

import React, { Component } from "react";
import CardItem from "./CardItem";
import { dataMovie } from "./dataMovie";

export default class RenderWithMap extends Component {
  // key
  renderMovieList = () => {
    return dataMovie.map((item, index) => {
      return (
        <div key={index} className="col-3 p-2">
          <CardItem movie={item} />
        </div>
      );
    });
  };
  render() {
    return (
      <div className="row container mx-auto ">{this.renderMovieList()}</div>
    );
  }
}

import React, { Component } from "react";

export default class CardItem extends Component {
  render() {
    return (
      <div className="card text-left h-100">
        <img
          style={{
            height: "80%",
            objectFit: "cover",
          }}
          className="card-img-top"
          src={this.props.movie.hinhAnh}
          alt
        />
        <div className="card-body">
          <h4 className="">{this.props.movie.tenPhim}</h4>
          <button className="btn btn-danger">Mua ngay</button>
        </div>
      </div>
    );
  }
}
/**
 *  {
    maPhim: 1283,
    tenPhim: "Trainwreck",
    biDanh: "trainwreck",
    trailer: "https://www.youtube.com/embed/2MxnhBPoIx4",
    hinhAnh: "https://movienew.cybersoft.edu.vn/hinhanh/trainwreck.jpg",
    moTa: "Having thought that monogamy was never possible, a commitment-phobic career woman may have to face her fears when she meets a good guy.",
    maNhom: "GP00",
    ngayKhoiChieu: "2019-07-29T00:00:00",
    danhGia: 5,
  },
 */

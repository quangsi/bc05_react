import logo from "./logo.svg";
import "./App.css";
import DemoClass from "./DemoComponent/DemoClass";
import DemoFunction from "./DemoComponent/DemoFunction.jsx";
import Ex_Layout from "./Ex_Layout/Ex_Layout";
import DataBinding from "./DataBinding/DataBinding";
import DemoProps from "./DemoProps/DemoProps";
import RenderWithMap from "./RenderWithMap/RenderWithMap";
import DemoState from "./DemoState/DemoState";
import Ex_Car from "./Ex_Car/Ex_Car";
import ConditionalRendering from "./ConditionalRendering/ConditionalRendering";
import Ex_ShoeShop from "./Ex_ShoeShop/Ex_ShoeShop";
import Demo_Mini_Redux from "./Demo_Mini_Redux/Demo_Mini_Redux";
import Ex_ShoeShop_Redux from "./Ex_ShoeShop_Redux/Ex_ShoeShop_Redux";
import Ex_Tai_Xiu from "./Ex_Tai_Xiu/Ex_Tai_Xiu";
import LifeCycle from "./LifeCycle/LifeCycle";
import Ex_Form from "./Ex_Form/Ex_Form";
// import RenderWithMap from "./RenderWithMap/RenderWithMap";
function App() {
  return (
    <div className="App">
      {/* <DemoClass /> */}
      {/* <DemoFunction></DemoFunction> */}
      {/* <Ex_Layout /> */}
      {/* <RenderWithMap /> */}
      {/* <DataBinding /> */}
      {/* <DemoProps /> */}
      {/* <RenderWithMap /> */}
      {/* <DemoState /> */}
      {/* <Ex_Car /> */}
      {/* <ConditionalRendering /> */}
      {/* <Ex_ShoeShop /> */}
      {/* <Demo_Mini_Redux /> */}
      {/* <Ex_ShoeShop_Redux /> */}
      {/* <Ex_Tai_Xiu /> */}
      {/* <LifeCycle /> */}
      <Ex_Form />
    </div>
  );
}

export default App;

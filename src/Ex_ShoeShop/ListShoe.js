import React, { Component } from "react";
import ItemShoe from "./ItemShoe";

export default class ListShoe extends Component {
  renderListShoe = () => {
    return this.props.shoeArr.map((item) => {
      return (
        <ItemShoe
          handleAddToCart={this.props.handleAddToCart}
          handleViewDetail={this.props.handleChangDetail}
          data={item}
        />
      );
    });
  };
  render() {
    return <div className="row">{this.renderListShoe()}</div>;
  }
}
// npm i  -  npm start

// redux

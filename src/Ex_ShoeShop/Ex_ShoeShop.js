import React, { Component } from "react";
import Cart from "./Cart";
import { dataShoe } from "./dataShoe";
import DetailShoe from "./DetailShoe";
import ItemShoe from "./ItemShoe";
import ListShoe from "./ListShoe";

export default class Ex_ShoeShop extends Component {
  state = {
    shoeArr: dataShoe,
    detail: dataShoe[1],
    cart: [],
  };

  handleChangDetail = (value) => {
    this.setState({ detail: value });
  };
  handleAddToCart = (shoe) => {
    let cloneCart = [...this.state.cart];
    let index = this.state.cart.findIndex((item) => {
      return item.id == shoe.id;
    });
    if (index == -1) {
      // th2: sp chưa có trong giỏ hàng
      let cartItem = { ...shoe, number: 1 };
      cloneCart.push(cartItem);
    } else {
      // th1 đã có
      cloneCart[index].number++;
    }
    this.setState({
      cart: cloneCart,
    });
    // th1 : sp đã có trong giỏi hàng => tăng key number
    // th2 : sp chưa có => tạo mới và push
  };

  render() {
    return (
      <div className="container">
        {/* cart */}
        <Cart cart={this.state.cart} />
        <ListShoe
          handleAddToCart={this.handleAddToCart}
          handleChangDetail={this.handleChangDetail}
          shoeArr={this.state.shoeArr}
        />
        <DetailShoe detail={this.state.detail} />
        {/* xem chi tiet */}
      </div>
    );
  }
}
// life cycle

let a = 2;
let b = a;
let c = b;

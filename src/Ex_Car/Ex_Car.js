import React, { Component } from "react";

export default class Ex_Car extends Component {
  state = {
    img: "./img/CarBasic/products/red-car.jpg",
  };
  handleChangeColor = (color) => {
    this.setState({
      img: `./img/CarBasic/products/${color}-car.jpg`,
    });
  };
  render() {
    return (
      <div className="container py-5">
        <div className="row">
          <img src={this.state.img} alt="" className="col-4" />
          <div>
            <button
              onClick={() => {
                this.handleChangeColor("black");
              }}
              className="btn btn-dark"
            >
              Black
            </button>
            <button
              onClick={() => {
                this.handleChangeColor("red");
              }}
              className="btn btn-danger mx-5"
            >
              Red
            </button>
            <button
              onClick={() => {
                this.handleChangeColor("silver");
              }}
              className="btn btn-secondary"
            >
              Silver
            </button>
          </div>
        </div>
      </div>
    );
  }
}

import React, { Component } from "react";
import { connect } from "react-redux";
import { CHOOSE_OPTION, TAI, XIU } from "../redux/xucXacContant";
let styles = {
  btn: {
    width: 150,
    height: 150,
    fontSize: 50,
  },
};
class XucXac extends Component {
  renderMangXucXac = () => {
    return this.props.mangXucXac.map((item) => {
      return (
        <img style={{ width: "100px", margin: 30 }} src={item.img} alt="" />
      );
    });
  };
  render() {
    return (
      <div className="d-flex justify-content-between container">
        <button
          onClick={() => {
            this.props.handleChooseOption(TAI);
          }}
          style={styles.btn}
          className="btn btn-danger"
        >
          Tài
        </button>
        <div>{this.renderMangXucXac()}</div>
        <button
          onClick={() => {
            this.props.handleChooseOption(XIU);
          }}
          style={styles.btn}
          className="btn btn-dark"
        >
          Xỉu
        </button>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    mangXucXac: state.xucXacReducer.mangXucXac,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleChooseOption: (luaChon) => {
      dispatch({
        type: CHOOSE_OPTION,
        payload: luaChon,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(XucXac);

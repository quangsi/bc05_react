import { CHOOSE_OPTION, PLAY_GAME } from "./xucXacContant";

let initialState = {
  mangXucXac: [
    {
      img: "./imgXucXac/1.png",
      giaTri: 1,
    },
    {
      img: "./imgXucXac/1.png",
      giaTri: 1,
    },
    {
      img: "./imgXucXac/1.png",
      giaTri: 1,
    },
  ],
  luaChon: null,
  soLuotThang: 0,
  soLuotChoi: 0,
};
export const xucXacReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case PLAY_GAME: {
      // random from to js
      let newMangXucXac = state.mangXucXac.map((item) => {
        let random = Math.floor(Math.random() * 6) + 1;
        return {
          img: `./imgXucXac/${random}.png`,
          giaTri: random,
        };
      });
      state.mangXucXac = newMangXucXac;
      // tính toán
      console.log(
        `  🚀 __ file: XucXacReducer.js:32 __ xucXacReducer __ newMangXucXac`,
        newMangXucXac
      );

      return { ...state };
    }
    case CHOOSE_OPTION: {
      return {
        ...state,
        luaChon: payload,
      };
    }
    default:
      return state;
  }
};

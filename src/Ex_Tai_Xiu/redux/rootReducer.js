import { combineReducers } from "redux";
import { xucXacReducer } from "./XucXacReducer";

export const rootReducer_XucXac = combineReducers({
  xucXacReducer: xucXacReducer,
});

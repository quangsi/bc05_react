import React, { Component } from "react";
import { connect } from "react-redux";
import { PLAY_GAME } from "../redux/xucXacContant";

class KetQua extends Component {
  render() {
    return (
      <div className="text-center">
        <button onClick={this.props.handlePlayGame} className="btn btn-warning">
          Play Game
        </button>
      </div>
    );
  }
}
let mapDispatchToProps = (dispatch) => {
  return {
    handlePlayGame: () => {
      dispatch({ type: PLAY_GAME });
    },
  };
};
export default connect(null, mapDispatchToProps)(KetQua);

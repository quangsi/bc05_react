import React, { Component } from "react";
import KetQua from "./KetQua/KetQua";
import XucXac from "./XucXac/XucXac";
import bg_game from "../assets/bgGame.png";
import "./game.css";
export default class Ex_Tai_Xiu extends Component {
  render() {
    return (
      <div
        className="font_game py-5"
        style={{
          backgroundImage: `url(${bg_game})`,
          width: "100vw",
          height: "100vh",
        }}
      >
        <XucXac />
        <KetQua />
      </div>
    );
  }
}
// <11 xỉu
// >= 11 tài

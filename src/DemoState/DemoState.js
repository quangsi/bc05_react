import React, { Component } from "react";

export default class DemoState extends Component {
  state = {
    like: 0,
  };
  handlePlusLike = () => {
    // this.state.like = 10;
    // setState ~ bất đồng bộ
    this.setState(
      {
        like: this.state.like + 1,
      },
      () => {
        console.log("thành công", this.state.like);
      }
    );

    console.log(this.state.like);
    // console.log(this.state.like);
  };
  render() {
    return (
      <div>
        <p className="display-4">{this.state.like}</p>
        <button onClick={this.handlePlusLike} className="btn btn-success">
          Plus like
        </button>
      </div>
    );
  }
}
